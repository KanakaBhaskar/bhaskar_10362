/**********************************************************
AUTHOR:

P.KANAKA BHASKAR  Emp-Id:10362   pedapudibhaskar@gmail.com  9966539647

In this Program we are reversing the float numbers.

 ***********************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

int main() {
	//character array for storing the float numbers
	char str[20];
	int count=0,number=0,i;//instant variables

	printf("enter the float value:");
	scanf("%s",str);

	//counting the digits after '.'
	for(i=strlen(str)-1;i>=0;i--) {
		if(str[i]!='.')
			count++;
		else 
			break;
	}
	//converting the float array into integer number.
	for(i=0;i<strlen(str);i++) {
		if(str[i]>='0' && str[i]<='9')
			number=(number*10)+(str[i]-48);
	}
	for(i=0;i<strlen(str);i++) {
		if(i==count) {
			str[i]='.';
		}
		else {
			str[i]=(number%10)+48;
			number=number/10;
		}
	}
	printf("The reversed number = %s\n",str);
}

/********************************************************

OUTPUT:
enter the float value:1234.567
The reversed number = 765.4321

**********************************************************/
