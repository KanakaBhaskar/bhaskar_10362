/****************************************************************************************************** 
  	
	
  	
	Bhaskar Pedapudi	10362	pedapudibhaskar@gmail.com		Cell : 9966539647

        Purpose:
        --------

        --To Communicate between server and client using sharedmemory and threads
        --client uses pthreads and mutex concept

******************************************************************************************************/


#include<iostream>
#include<unistd.h>
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <stdio.h> 
#include<pthread.h>
#include<stdlib.h>


using namespace std; 

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static char *clientname;
static int shmid; 

class ClientForSharedMemory {

	public :

	char *str;
	pthread_t thread1,thread2,thread3;
        //static int shmid; 

	ClientForSharedMemory() {

		
		key_t key = ftok("sharedmemoryfile",65);
		// shmget retruns an identifier in shmid
		shmid = shmget(key,1024,0666|IPC_CREAT); 


		str = (char*) shmat(shmid,(void*)0,0); 

		cout<<str<<endl;
		shmdt(str);
	}

	//method for entering the clientsname
	static void *clientName(void *p) {
		// In this program we are using mutex to provide the lock system and to avoid collision among the threads		

		pthread_mutex_lock( &mutex );

		cout<<"Person name :"<<endl;what is your name

		clientname=(char*) shmat(shmid,(void*)0,0); 

		cin>>clientname;

		sleep(20);
		//here we are supposed to release the lock
		pthread_mutex_unlock( &mutex );
		

	}

	//metod to create three client threads
	void create_clientThreads() {

		pthread_create( &thread1, NULL,clientName,NULL);
		pthread_create( &thread2, NULL,clientName,NULL);
		pthread_create( &thread3, NULL,clientName, NULL);

        }

	//method for joining  all the threads
	void joinAllThreads() {
		pthread_join( thread1, NULL);
    		pthread_join( thread2, NULL);
		pthread_join( thread3, NULL);

	}	


};	
//static member initialization

//char* ClientForSharedMemory :: str = NULL;
//int ClientForSharedMemory :: shmid =0;
//clientName = NULL;
int main() {
	//creating object for clientforsharedmemory
	ClientForSharedMemory c;
        c.create_clientThreads();
	sleep(10);
	//calling the joiningclientthread methid
	c.joinAllThreads();
	return 0;
}

/************************************************

OUTPUT:


what is your name
Person name :
bhaskar
Person name :
Kanaka
Person name :
Bobby

********************************************/

