/********************************************************************************************************************
  NAME                  PHONE NUMBER           EMAIL ID			      EMPLOYEE-ID
___________________________________________________________________________________________________________________
 P.KANAKA BHASKAR       9966539647	   pedapudibhaskar@gmail.com		10362
___________________________________________________________________________________________________________________
In this program we rotate the matrix in the angle of 90 degrees

*******************************************************************************************************************/

 
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
int main(){
	//declaring the variables
	int i,j,k,row,column;
	srand(getpid());
	//entering the rows and coloumns
	printf("Enter row value  ");
	scanf("%d",&row);
	printf("Enter colums value  ");
	scanf("%d",&column);
	//declaring the resultant matrix
	int arr1[column][row];
	//declaring the input matrix
	int arr[row][column];
		for(i=0;i<row;i++){
			for(j=0;j<column;j++){
				//generating the elements randomly by using rand function
				arr[i][j]= random()%10;
				
		}
	}
	//displaying the matrix before rotation
	printf("matrix before rotation:");
	printf("\n");
	for(i=0;i<row;i++){
		for(j=0;j<column;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	//logic for rotating the matrix
	for(i=0,j=row-1;i<row && j>=0;i++,j--){
			for(k=0;k<column;k++)
				arr1[k][j]=arr[i][k];
			}
	//displaying the matrix after rotation
	printf("matrix after rotation:");
	printf("\n");
	for(i=0;i<column;i++){
		for(j=0;j<row;j++){
			printf("%d ",arr1[i][j]);
		}
		printf("\n");
	}
	return 0;

}
/***************************************************************************************
OUTPUT:

user@user:~/assignments$ ./a.out
Enter row value  5
Enter colums value  5
matrix before rotation:
6 5 0 6 7 
9 8 7 8 2 
5 4 2 2 5 
0 5 1 3 0 
5 5 9 5 1 
matrix after rotation:
5 0 5 9 6 
5 5 4 8 5 
9 1 2 7 0 
5 3 2 8 6 
1 0 5 2 7
user@user:~/assignments$ ./a.out
Enter row value  7
Enter colums value  4
matrix before rotation:
2 0 1 8 
4 5 4 0 
6 7 4 3 
1 6 7 0 
1 9 6 9 
9 4 2 9 
8 7 1 8 
matrix after rotation:
8 9 1 1 6 4 2 
7 4 9 6 7 5 0 
1 2 6 7 4 4 1 
8 9 9 0 3 0 8 
******************************************************************************************************************/
