/***********************************************************************************
P.KANAKA BHASKAR   	Emp-Id:10362		pedapudibhaskar@gmail.com	Ph No: 9966539647

The purpose of this program is to write the content into the file by using system calls until and unless you press ctrl+c

**************************************************************************************/

#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>


int main() {

	int fp1;
	char buff[100];
	fp1=open("output1.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1) {

		printf("Error in opening the file  \n");
       		exit(0);	
	}
	else 
			printf("input file opened in read mode\n");
	printf("write the conent that should be written into file \n");
	//writing from input file to output file
	printf("enter the content and press ctrl+c to stop\n");
	while(1) {
	int size = read(0,buff,100);
	write(fp1,buff,size);
	}
	printf("writing to output file using system call successfull\n");
	return 0;

}

/***********************************OUTPUT**********************************************
./a.out
input file opened in read mode
write the conent that should be written into file
enter the content and press ctrl+c to stop
Hai this is Bhaskar
I am from Bhimavaram
I am working as an embedded engineer in Innominds
^C
***************************************************************************************/

