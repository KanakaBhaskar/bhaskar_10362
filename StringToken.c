/************************************************************
P.KANAKA BHASKAR  Emp-Id:10362   pedapudibhaskar@gmail.com  9966539647

This program is to implement an user defined string token

************************************************************/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main() {
	//taken a char array
	char string[]="BOBBY IS VERY GOOD BOY"; 
	//allocating memory for pointer
	char * token=(char *)malloc(strlen(string)*sizeof(char));
	//here we have taken space as delimeter
	char str[2]=" ";
	//instant variables used in for loop
	int i=0,j=0;
	//logic for seperate the word and put it on to the token
	for(i=0;i<strlen(string);i=j+1) {
		if(token!=NULL)
			j=i;
		while(string[j]!=str[0])  {
			if(string[j]=='\0') break;
			j++;
		}
		strncpy(token,string+i,j-i);
		token[j-i]='\0';
		printf("%s",token);
		printf("\n");
	}
	return 0;
}

/************************************************************

OUTPUT:
user@user:~/practiceclass.c/selfish/strings$ ./a.out
BOBBY
IS
VERY
GOOD
BOY

************************************************************/
