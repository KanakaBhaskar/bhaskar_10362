/****************************************************************************
Author:

Bhaskar Pedapudi	Emp id:10362	cell No: 9966539647
---------------------
purpose:
---------------------

This program is to perform sorting the elements in the given array with the use of Bubble sorting techinque.

 ****************************************************************************/


#include<stdio.h>
#include<iostream>
#include<unistd.h>

using namespace std;

/*This class contains methods related to bubblesort 
  They are performing some tasks like reading the elements into the array
  sorting the elements
  Dislay the array elements 
 */

class BubbleSort {

	//creating instant variable
	int size=0;
	//declaring the array
	int *array;

public:		
	//This is the parameterized constructor for allocating the memory dynamically for the array
	BubbleSort(int size) {
		array= new int[size];
	}

	//This method is used to reading the elements into the array
	void readingTheArrayElements(int size) {
		//reading the elements into the array

		cout<<"enter the array elements"<<endl;
		for(int i=0;i<size;i++) {
			cin>>array[i];
		}
	}


	//This method is used to display the array elements
	void displayArrayElements(int size) {
		cout<<"Before sorting...."<<endl;
		for(int i=0;i<size;i++) {
			cout<< array[i] << " ";
		}
		cout<<endl;
	}

	//This method is used to perform bubblesort on the given array

	void bubbleSort(int size) {
		//Bubble sort logic
		cout<<"After Sorting..."<<endl;
		for(int i=0;i<size;i++) {
			for(int j=0;j<size-1;j++) {
				if(array[j]>array[j+1]) {
					int temp=array[j];
					array[j]=array[j+1];
					array[j+1]=temp;
				}
			}
		}

		for(int i=0;i<size;i++) {
			cout<<array[i]<<" ";
		}
		cout<<endl;
	}
};




int main(void) {
	int size;
	cout<<"enter the size of the array"<<endl;
	cin>>size;
	if(size<=0) {
		cout<<"The size should be greater than zero"<<endl;
		exit(0);
	}
	//creating the object for the BubbleSort
	BubbleSort bubblesort(size);
	bubblesort.readingTheArrayElements(size);
	bubblesort.displayArrayElements(size);
	bubblesort.bubbleSort(size);

}

/**************************************************************************

OUTPUT :

enter the size of the array
5
enter the array elements
-9
5
66
1
0
Before sorting....
-9 5 66 1 0 
After Sorting...
-9 0 1 5 66 

***************************************************************************/





