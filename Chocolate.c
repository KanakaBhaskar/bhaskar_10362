/***********************************************************************************************************************
 NAME                           EMAIL-ID                       PHONE NUMBER                        EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------------------
PEDAPUDI BHASKAR             pedapudibhaskar@gmail.com         9966539647                            10368
------------------------------------------------------------------------------------------------------------------------
This program is implementation of counting the maximum number of chocolates one can get after getting 30% cash back
************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int main(void) {
	int cost,money,total=0,total1=0,discount=0;
	printf("enter the cost of the chocolate\n");
	scanf("%d",&cost);
	printf("enter the total money\n");
	scanf("%d",&money);
	discount=(int)(cost*30/100);
	total = money/cost;
	total1 = total+(total*discount/cost);
	printf("The total chocolates can be brought after discount = %d\n",total1);
}

/********************************************OUTPUT FOR THE ABOVE PROGRAM***********************************************
 enter the cost of the chocolate
10
enter the total money
100
The total chocolates can be brought after discount = 13
************************************************************************************************************************/
