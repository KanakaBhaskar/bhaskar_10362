
/*----------------------------------------------------------------------------------------------------------------------------------
      
	Bhaskar Pedapudi   :	Emp id -10362 :	pedapudibhaskar@gmail.com	:	Cell : 9966539647


       PURPOSE
       ---------
       This program is typically shows a Object oriented version of Family tree, Calcualtes the Father's
       income and savings of fathers and Son's salary compunding over certain years.

----------------------------------------------------------------------------------------------------------------------------------*/


#include<iostream>
#include<string.h>
#include<math.h>

using namespace std;
/*This person class contain data member and member functions*/  
class Person{

	protected:
		string name;
		int age;
		double salary;
		double compound_interest;
		double principle,rate_of_interest=10,no_of_years=3;
	public:
		Person(){
			compound_interest=0.0;
		}
		Person(string name,int age){
			this->name=name;
			this->age=age;
		}
		/* Method to calculate compound interest based on salary*/
		double cal_compound_int(double salary){
			compound_interest=((0.5*salary)*pow((1+(rate_of_interest)/100),no_of_years));
			cout<<"salary:"<<salary<<endl;
                  	cout<<"compound_interest for"<<"  "<<no_of_years<<"  years is: "<<compound_interest<<endl;
		}
                /* method used to display name and age*/
		void displayPersonDetails(){
			cout<<"name:"<<name<<endl;
			cout<<"age:"<<age<<endl;
		}
};

/* Father class which inherits from Person class*/
class Father : public Person{
	private:        
		double salary;
	public:
                Father() {  salary = 0.0; }
		Father(string name,int age,double salary) : Person(name,age){
			this->salary=salary;
         	}
		void displayFatherDetails(){
			Person :: displayPersonDetails();
			Person :: cal_compound_int(salary);
		}
		~Father(){
		}
};

/* Mother class which inherits from person class*/
class Mother : public Person{
	public:
		Mother(){
		}
		Mother(string name,int age) : Person(name,age){
		}
		void displayMotherDetails(){
			Person :: displayPersonDetails();
		}
		~Mother(){
		}
};

/* Son class which inherits from Father and Mother */
class Son : public Father,public Mother{
	private:
		double salary,compound_interest=0.0;
		double principle,rate_of_interest=0.4,no_of_years=2;
	public:
		Son(){
		}
		Son(string name,int age,double salary) : Father(name,age,salary){
			this->salary=salary;
		}
		void displaySonDetails(){
			Father :: displayPersonDetails();
			Father :: cal_compound_int(salary);
		}
		~Son(){
		}
};

/* Daughter class which inherits from Father and Mother*/
class Daughter : public Father,public Mother{
	private:
		string university;
	public:
		Daughter(){
		}

		Daughter(string name,int age,string university) : Mother(name,age){
			this->university=university;
		}
		void displayDaughterDetails(){
			Mother :: displayPersonDetails();
			cout<<"university:"<<university<<endl;
		}
		~Daughter(){
		}
};

/* In Family class we are creating objects for the Father,Mother,Son and Daughter class*/  
class Family{
	public:
		Family(){	
			Father father("SATYANARAYANA",48,50000.00);
			Mother mother("PARVATHI",39);
			Son son("BHASKAR",21,25000.00);
			Daughter daughter("CHETHANA",18,"ANDHRA UNIVERSITY");
		        cout<<endl<<"......*Father Details*......."<<endl;
			father.displayFatherDetails();
			cout<<endl<<"......*Mother Details*......."<<endl;
			mother.displayMotherDetails();
			cout<<endl<<"......*Son Details*....."<<endl;
			son.displaySonDetails();
			cout<<endl<<"......*Daughter Details*....."<<endl;
			daughter.displayDaughterDetails();
		}
};
/*object is created for the Family class*/
int main(){

	Family family;
}






/*******************************************************************

OUTPUT


......*Father Details*.......
name:SATYANARAYANA
age:48
salary:50000
compound_interest for  3  years is: 33275

......*Mother Details*.......
name:PARVATHI
age:39

......*Son Details*.....
name:BHASKAR
age:21
salary:25000
compound_interest for  3  years is: 16637.5

......*Daughter Details*.....
name:CHETHANA
age:18
university:ANDHRA UNIVERSITY
***********************************************************/
