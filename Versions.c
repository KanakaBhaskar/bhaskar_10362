/************************************************************
  P.KANAKA BHASKAR  Emp-Id : 10362    pedapudibhaskar@gmail.com  9966539647

  This program is to find out the latest version of any model based on version number.

 **************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(void) {

	char string1[100],string2[100];

	printf("enter the version1\n");
	gets(string1);

	printf("enter the version2\n");
	gets(string2);

	//comparing the two versions if string if string1 is greater then it is the newer version

	if(strcmp(string1,string2) > 0) {
		printf("The newer version is %s\n ",string1);
	}

	//comparing the two versions if string if string2 is greater then it is the newer version
	else if(strcmp(string1,string2) < 0) {
		printf("The newer version is %s\n ",string2);
	}

	else {
		printf("these two are equal\n");
	}

}
/*************************************************************
OUTPUT: 

enter the version1
1.2.3
enter the version2
1.01.3
The newer version is 1.2.3
*************************************************************/
