/*************************************************************
  P.KANAKA BHASKAR    Emp-Id:10362     pedapudibhaskar@gmail.com  Ph No:9966539647

  This program is to find the binary value of the decimal number using recursion

 ************************************************************/


#include<stdio.h>
#include<stdlib.h>

//This function is used to convert the decimal to binary using recursion

decimalToBinary(int num) {
	if(num==0)
		return 0;
	decimalToBinary(num/2);
	printf("%d",num%2);
	
}

int main() {
	//instant variables
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	printf("The binary equivalent is:");
	decimalToBinary(num);
	printf("\n");
	return 0;
}

/*************************************************************
OUTPUT:

enter the number
10
The binary equivalent is:1010

*******************************************************/
