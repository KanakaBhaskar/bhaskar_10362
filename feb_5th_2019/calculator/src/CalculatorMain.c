#include<headers.h>

int main() {

	//declaring the local variables
	int data1,data2,result,choice;
	int *pointer=&result;

	while(1) {
		//displaying the menu of simple calculator operations
		printf("***SIMPLE OPERATIONS OF CALCULATOR***\n");
		printf("1.ADDITION\n 2.SUBTRACTION\n 3.MULTIPLICATION\n 4.DIVISION\n 5.QUIT\n");
		//reading the user's choice
		printf("enter your choice\n");
		scanf("%d",&choice);
		//navigating according to user's choice
		switch(choice) {
			//reading the data
			case 1:	printf("enter the two numbers you want to add\n");
				scanf("%d %d",&data1,&data2);
				//calling the addTwoNumbers function				
				addTwoNumbers(data1,data2,pointer);
				printf("Addition of %d and %d is %d\n",data1,data2,result);
				break;

			case 2:	
				//reading the data
				printf("enter the two numbers you want to subtract\n");
				scanf("%d %d",&data1,&data2);
				//calling the subtractTwoNumbers function				
				subtractTwoNumbers(data1,data2,pointer);
				printf("Subtraction of %d and %d is %d\n",data1,data2,result);
				break;

			case 3:	

				//reading the data
				printf("enter the two numbers you want to multiply\n");
				scanf("%d %d",&data1,&data2);
				//calling the multiplyTwoNumbers function				
				multiplyTwoNumbers(data1,data2,pointer);
				printf("product of %d and %d is %d\n",data1,data2,result);
				break;

			case 4:	

				//reading the data
				printf("enter the two numbers you want to division\n");
				scanf("%d %d",&data1,&data2);
				//re-reading the data if it is invalid
				while(data2==0) {	
					printf("can't divide by zero\n");
					printf("enter a valid number:");
					scanf("%d",&data2);
				}
				//calling the divideTwoNumbers function				
				divideTwoNumbers(data1,data2,pointer);
				printf("Division of %d and %d is %d\n",data1,data2,result);
				break;
				//exiting from main
			case 5:	printf("exiting.....\n");
				exit(0);
				//default case
			default: printf("Invalid choice.... please enter valid one\n");
		}
	}

}

