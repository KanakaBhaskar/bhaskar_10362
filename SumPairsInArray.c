/************************************************************
P.KANAKA BHASKAR    Emp-id:10362  pedapudibhaskar@gmail.com  9966539647

In this program we are giving one number
And search for if that number is sum of any two numbers in the given array or not
If exits then it returns the array indeces of that respective numbers   
If not it returns NULL
************************************************************/
#include<stdio.h>
#include<stdlib.h>
//declaring the getArray function
int* getArray(int*,int,int);

int main() {
	int i; //instant variable used in for loop
	int array[10]={1,2,3,4,5,6,7,8,9,10};
	int *array1=NULL,num;

	printf("enter the number to searched\n");
	scanf("%d",&num);

	array1=getArray(array,10,num);
	if(array1 == NULL) {
	printf("there is no such number exists\n");
	exit(0);
	}
	//printf("The indeces are");
	for(i=0;i<2;i++) {
		printf("%d ",array1[i]);
			}
	return 0;
}
//This function is receiving array,its length,and the number to be searched.
int* getArray(int *a , int len,int num) {
	int *array=NULL;
	int i,j;
	//allocating memory for array to store the indeces
	array = (int*) malloc(2*(sizeof(int)));
	//logic for finding sum of two numbers in an array
	for(i=0,j=len-1;i<=j;i++,j--) {
		int temp1 = a[i]+a[j];

		if(temp1 < num) {
			j++;
			
		}
		else if(temp1 > num) {
			i--;
		}

		else {
			array[0]=i;
			array[1]=j;
			return array;
		}

	}
	return NULL;
}


/****************************************
OUTPUT: 

enter the number to searched
32
there is no such number exists
user@user:~/bhaskar.c$ ./a.out
enter the number to searched
10
The indeces are :0 9
********************************/
