/****************************************************************************
Author:

Bhaskar Pedapudi	Emp id:10362	cell No: 9966539647
---------------------
purpose:
---------------------

This program is to perform sorting the elements in the given array with the use of Insertion sorting techinque.

 ****************************************************************************/


#include<stdio.h>
#include<iostream>
#include<unistd.h>

using namespace std;

/*This class contains methods related to Insertion 
  They are performing some tasks like reading the elements into the array
  sorting the elements
  Dislay the array elements 
 */


class InsertionSort {


	//creating instant variable
	int size=0;
	int *array;
	public:	
	//This is the parameterized constructor for allocating the memory dynamically for the array
	InsertionSort(int size) {
		array= new int[size];
	}

	//This method is used to reading the elements into the array
	void readingTheArrayElements(int size) {
		//reading the elements into the array
		cout<<"enter the array elements"<<endl;
		for(int i=0;i<size;i++) {
			cin>>array[i];
		}
	}

	//This method is used to display the array elements
	void displayArrayElements(int size) {
		cout<<"Before sorting...."<<endl;
		for(int i=0;i<size;i++) {
			cout<<array[i]<<" ";
		}
		cout<<endl;
	}

	//This method is used to perform insetionsort on the given array
	void insertionSort(int size) {
		cout<<"After Sorting..."<<endl;
		//insertion sort logic
		for(int i=1;i<size;i++ ) {
			int j=i-1;
			int temp=array[i];
			while(j>=0 && array[j]>temp) {
				array[j+1]=array[j];
				j--;
			}
			array[j+1]=temp;
		}

		for(int i=0;i<size;i++) {
			cout<<array[i]<<" ";
		}
		cout<<endl;
	}
};



int main(void) {
	int size;
	cout<<"enter the size of the array"<<endl;
	cin>>size;
	if(size<=0) {
		cout<<"size should be greater than zero"<<endl;
		exit(0);
	}

	//creating the object for the InsertionSort
	InsertionSort insertion(size);
	insertion.readingTheArrayElements(size);
	insertion.displayArrayElements(size);
	insertion.insertionSort(size);

}




/*********************************************************************

OUTPUT:

enter the size of the array
4
enter the array elements
22
3
-1
6
Before sorting....
22 3 -1 6 
After Sorting...
-1 3 6 22 


 *********************************************************************/
