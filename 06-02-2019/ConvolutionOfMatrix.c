/************************************************************************************************************
    NAME 	      EMPLOYEE-ID			EMAIL-ID 			MOBILE NUMBER
------------------------------------------------------------------------------------------------------------
P.KANAKA BHASKAR	 10362			pedapudibhaskar@gmail.com		9966539647
-----------------------------------------------------------------------------------------------------------
This program is the implementation of convolution of two matrixes

*********************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#define SIZE 3
//function for printing the matrix
void printingMatrixElements(int rows,int columns,int arr[rows][columns]){
	int i,j;
	
	for(i=0;i<rows;i++){
		for(j=0;j<columns;j++){
			printf("%d\t",arr[i][j]);
		}
		printf("\n");
	}
}

int main(){
	int row,column,i,j,sum=0,k,l;
	srand(getpid());
	printf("Enter row and column values\n");
	scanf("%d %d",&row,&column);
	//declaring the main matrix
	int arr[row][column];
	//declaring the resultant matrix 
	int resultant[row][column];
	//declaring the window matrix
	int window[SIZE][SIZE];
	for(i=0;i<row;i++){
		for(j=0;j<column;j++){
			//generating the main matrix elements randomly
			arr[i][j]=rand()%10;
			resultant[i][j] = 0;
		}
	}
	printf("Displaying the main matrix\n");
	printingMatrixElements(row,column,arr);
	printf("Displaying the resultant matrix before convolution\n");
	printingMatrixElements(row,column,resultant);
	for(i=0;i<SIZE;i++){
		for(j=0;j<SIZE;j++){
			//generating the window matrix elements randomly
			window[i][j]=rand()%10;
		}
	}
	
	printf("Displaying the window matrix\n");
	printingMatrixElements(SIZE,SIZE,window);
	//logic for convolution
	for(i=0;i<row-2;i++){
		for(j=0;j<column-2;j++){
			sum = 0;
			for(k=0;k<=2;k++){
				for(l=0;l<=2;l++){
					sum+=arr[i+k][j+l]*window[k][l];
				}
			}
			resultant[i+1][j+1]=sum;
		}
		
	}
	printf("Displaying the resultant matrix after convolution\n");
	printingMatrixElements(row,column,resultant);
	return 0;
}
/************************************OUTPUT OF THE ABOVE PROGRAM**************************************
Enter row and column values
5
5
Displaying the main matrix
4	6	3	4	0	
2	0	8	0	6	
4	0	5	3	4	
8	8	8	5	3	
3	3	3	1	8	
Displaying the resultant matrix before convolution
0	0	0	0	0	
0	0	0	0	0	
0	0	0	0	0	
0	0	0	0	0	
0	0	0	0	0	
Displaying the window matrix
7	6	3	
0	0	0	
7	6	3	
Displaying the resultant matrix after convolution
0	0	0	0	0	
0	116	111	110	0	
0	166	167	169	0	
0	91	81	116	0	
0	0	0	0	0	
*********************************************************************************************************/ 
