/************************************************************************************************************
    NAME 		Emp-Id 			EMAIL-ID 			Mobile
------------------------------------------------------------------------------------------------------------
P.KANAKA BHASKAR	 10362			pedapudibhaskar@gmail.com	9966539647
------------------------------------------------------------------------------------------------------------

In this program we perform the multiplication of two matrices. Performing multipication only if columns of first matrix equal to 
rows of second matrix.

**********************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

//function to print the elements of the matrix
void printingMatrixElements(int rows,int columns,int arr[rows][columns]){
        int i,j;
        for(i=0;i<rows;i++){
                for(j=0;j<columns;j++){
                        printf("%d\t",arr[i][j]);
                }
                printf("\n");
        }
}

int main(){
	//declaring the variables
        int row,column,i,j,sum,k,row1,column1;
        srand(getpid());
	//reading the row and column values of the first matrix
        printf("Enter row and column values of first matrix \n");
        scanf("%d %d",&row,&column);
	//reading the row and column values of the second matrix
        printf("Enter row and column value of second matrix\n");
        scanf("%d %d",&row1,&column1);
	if(column!=row1){//condition to check wheather matrix multiplication is possible or not
		printf("it is not possible to multiply");
		}else{
			srand(getpid());
			//declaring the arrays
			int matrix[row][column];
			int matrix1[row1][column1];
			int resultant[row][column1];
			//assigning the values for first matrix
			for(i=0;i<row;i++){
				for(j=0;j<column;j++){
					matrix[i][j]=rand()%10;
				}
			}
			printf("printing the first matrix\n");
			printingMatrixElements(row,column,matrix);
			//assigning the values for the second matrix
			for(i=0;i<row1;i++){
				for(j=0;j<column1;j++){
					matrix1[i][j]=rand()%10;
				}
			}
			printf("printing the second matrix\n");
			printingMatrixElements(row1,column1,matrix1);
			
			for(i=0;i<row;i++){
				for(j=0;j<column1;j++){
					resultant[i][j]=0;
				}
			}
			//printing the matrix elements before multiplication
			printf("printing the resultant matrix before multiplication\n");
			printingMatrixElements(row,column1,resultant);
			//logic for matrix multiplication
			for(i=0;i<row;i++){
				for(j=0;j<column1;j++){
					sum=0;
					for(k=0;k<column;k++){
						sum+=matrix[i][k]*matrix1[k][j];
					}
					resultant[i][j]=sum;
				}
			}

			printf("printing the resultant matrix after multiplication\n");
			printingMatrixElements(row,column1,resultant);

		}
       return 0;	
}

/*******************************************OUTPUT***************************************************************
Enter row and column values of first matrix 
2
3
Enter row and column value of second matrix
3
2
printing the first matrix
1	1	9	
7	1	2	
printing the second matrix
1	6	
3	6	
7	0	
printing the resultant matrix before multiplication
0	0	
0	0	
printing the resultant matrix after multiplication
67	12	
24	48	 
****************************************************************************************************************************************/
