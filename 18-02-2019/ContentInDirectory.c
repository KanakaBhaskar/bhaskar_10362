/*********************************************************************************************************************
 NAME                        EMPLOYEEID                             EMAILID                         PHONENUMBER
 ---------------------------------------------------------------------------------------------------------------------
 P.KANAKA BHASKAR              10362                               pedapudibhaskar@gmail.com         9966539647
 ---------------------------------------------------------------------------------------------------------------------
This program is the implemenation of displaying the directories in the file
***********************************************************************************************************************/


#include<stdio.h>
#include <sys/types.h>
#include <dirent.h>
int main(int argc,char**argv){
	DIR *dp;
	struct dirent *ptr;
	dp = opendir(argv[1]);
	while(ptr=readdir(dp)){
		printf("%s\n",ptr->d_name);
	}
	closedir(dp);
	return 0;
}
/**************************************************output of the above program**************************************************
vidya@vidya-Inspiron-5558:~/tataji$ ./a.out chatting\ through\ socketprogramming/
.
serverside
..
clientside
********************************************************************************************************************************/
 
