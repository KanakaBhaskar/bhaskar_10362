/****************************************************************************************************** 



  Bhaskar Pedapudi  : 10362	pedapudibhaskar@gmail.com		Cell : 9966539647

Purpose:
--------

Enter n number of student details and display all student details
also display students by serach through branch and university

 ******************************************************************************************************/

#include<iostream>
#include<vector>
#include<stdlib.h>
using namespace std;

class Student;
//creating university class 
class University {

	//declaring the data members
	string university_name;
	vector<Student> s;

	public:
	//getters for university name and branch
	string getUniversityName()
	{ 
		return university_name;
	}

	//default constructor for University class
	University() {

		cout<<"enter student university(geetham/andhra/jntuk):"<<endl;
		cin>>university_name;	
	}


	//displaying students information
	void displayUniversityDetails() {

		cout<<"\t\tuniversity name:"<<university_name<<endl;

	}



};

class Student : public University {


	int student_id;
	string student_name;
	int student_contact;
	string student_branch;
	public:	
	Student() {

		cout<<"enter student id :"<<endl;
		cin>>student_id;
		cout<<"enter student name :"<<endl;
		cin>>student_name;
		cout<<"enter student contact :"<<endl;
		cin>>student_contact;
		cout<<"enter student branch(EEE/ECE/CSE/MECH/CIVIL/IT) :"<<endl;
		cin>>student_branch;

	}
	string getBranch() {
		return student_branch;
	}		
	void displayStudentDetails() {
		cout<<"\t\tstudent_id:"<<student_id<<endl;
		cout<<"\t\tstudent name:"<<student_name<<endl;
		cout<<"\t\tstudent contact:"<<student_contact<<endl;
		cout<<"\t\tstudent branch:"<<student_branch<<endl;
		University::displayUniversityDetails();
	}
};
//method to search by university-name/branch
void search(vector<Student> s,string name) {

	int flag=0;


	for ( auto it : s) { 

		if(it.getUniversityName()==name || it.getBranch()==name) {
			flag++;
			cout<<"\t\t*****student-"<<flag<<" details*****"<<endl;
			it.displayStudentDetails();
			cout<<"\t\t-------------------------\n"<<endl;

		}
	}
	if(!flag) {
		cout<<"\t\t**********no details to display**********\n\n"<<endl;
	}
}

//main function 
int main() {

	//declaring the local variables
	int choice;
	string name;

	vector<Student> s;



	while(1) {
		//displaying the menu page
		cout<<"1.add student\n2.display all students\n3.display by university name\n4.display by branch\n5.quit"<<endl;

		//reading the user's choice
		cout<<"enter your choice:"<<endl;
		cin>>choice;

		//navigating according to user's choice		
		switch(choice) {


			case 1:
				{
					Student stu;
					s.push_back(stu);
				}
				break;
			case 2:


				for ( auto it : s) {
					cout<<"\t\t*****student details*****"<<endl;
					it.displayStudentDetails();
					cout<<"\t\t-------------------------\n"<<endl;
				}

				break;
			case 3:
				cout<<"enter university name(geetham/andhra/jntuk) to be search:"<<endl;
				cin>>name;
				search(s,name);
				break;
			case 4:
				cout<<"enter student branch(EEE/ECE/CSE/MECH/CIVIL/IT) to be search:"<<endl;
				cin>>name;
				search(s,name);
				break;
			case 5:
				exit(0);
		}



	}    
	return 0;
}
/*================================================================================
// OutPut
//---------------------------


1.add student
2.display all students
3.display by university name
4.display by branch
5.quit
enter your choice:
2
 *****student details*****
student_id:1
student name:bhaskar
student contact:123456789
student branch:ECE
university name:JNTUH
-------------------------

 *****student details*****
student_id:2
student name:hema
student contact:1234579635
student branch:CSE
university name:geetham
-------------------------

1.add student
2.display all students
3.display by university name
4.display by branch
5.quit
enter your choice:
4
enter student branch(EEE/ECE/CSE/MECH/CIVIL/IT) to be search:
CSE
 *****student-1 details*****
student_id:2
student name:hema
student contact:12345679635
student branch:CSE
university name:geetham
-------------------------

 */





