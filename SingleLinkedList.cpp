/****************************************************************************
Author:

Bhaskar Pedapudi	Emp id:10362	cell No: 9966539647
---------------------
purpose:
---------------------

This program is to create a linked list perform Some operations on it

 ****************************************************************************/


#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<iostream>
#include<unistd.h>

//creating a structure for the data and self referential structure
struct EmployeeDetails {
	int employid;
	EmployeeDetails *next; //self referential structure
};

using namespace std;

//globally declared count variable
static int count=0;  


class SingleLinkedList{

	//this pointer is used to store the starting address of the single linked list
	struct EmployeeDetails *head=NULL;

	public:

	//this method is used to display all the employee id's
	void  displayEmployeeid() {

		EmployeeDetails  *temp=head;
		if(temp==NULL) {
			cout<<"The list is empty"<<endl;
		}
		while(temp!=NULL) {
			cout<<"The Employee id is: "<<temp->employid<<endl;
			temp=temp->next;
		}

	}

	//this method is used to delete the particular employee id in the single linked list
	void deleteEmployeeid(int deletenum) {

		//creating a structrued pointer and assign the head value to it
		EmployeeDetails *temp=head;
		//creating a structrued pointer and assign the temp value to it
		EmployeeDetails *prev = temp;

		//these two pointers are required for traversing purpose

		if(head==NULL) {
			cout<<"list is empty"<<endl;
		}

		//checking if the employ id to be deleted is head or not

		if(head->employid==deletenum) {
			head=head->next;
			delete temp;
			temp=NULL;
			return;
		}
		//for(temp=head;temp->employid!=deletenum;prev=temp,temp=temp->next); 
		if(deletenum>count) {
			cout<<"The number you have entered is not exist"<<endl;
			return;
		} 
		count=count-1;
		prev->next=temp->next;
		delete temp;
		temp=NULL;
	}

	//this method updates the data in which is present in specific position in the list
	void update(int position) {
		if(position<0 || position > count) {
			cout<<"cannot update the data at that position:"<<position<<endl;

		}
		int index;
		EmployeeDetails *temp=head;
		for(index=1;index<position;index++) {
			temp=temp->next;
		}
		cout<<"enter the data to be update:"<<endl;
		cin>>temp->employid;
	}


	//This method is used to add the employee id's to the linked list

	void add_node(int data , int position) {

		//this condition is for if we want to add at beginning of the linked list
		if(position == 0) {
			EmployeeDetails  *temp = new EmployeeDetails ;
			if(temp == NULL) {
				cout<<"memory creation is failed"<<endl;
			}
			else {
				temp->next=head;
				temp->employid = data;
				head=temp;
				count=count+1;
			}
		}

		//this condition is for if we want to add at the ending of the linked list

		else if(position == count) {
			EmployeeDetails *employee = new EmployeeDetails ;
			if(head==NULL) {
				employee->next=head;
				head=employee;
				head->employid = data;
				count =count+1;
			}
			else {
				EmployeeDetails  *temp=head;
				for(temp=head;temp->next!=NULL;temp=temp->next);
				temp->next=employee;
				employee->next=NULL;
				employee->employid=data;
				count=count+1;

			}
		}

		//this condition is for if we want to add at any specific position
		else {    
			int count1=1;
			EmployeeDetails *employee = new EmployeeDetails;
			EmployeeDetails *temp=head;
			while((count1)!=position)
			{
				temp=temp->next;
				count1=count1+1;
			}
			employee->employid = data;
			employee->next=temp->next;
			temp->next=employee;

		}
	}


};

int main(void) {
	//instant variable
	int number;
	//creating object for the class
	SingleLinkedList sll;
	int position;
	while(true)
	{
		cout<<"what operation do you want to perform"<<endl;
		cout<<"1.Add\n2.Delete\n3.Update\n4.Display\n "<<endl;
		cin>>number;
		switch(number) {
			case 1:		cout<<"enter the position the node to be added"<<endl;

					cin>>position;
					if(position>=0 && position<=count) {
						cout<<"enter the employe id to be added"<<endl;
						int employid;
						cin>>employid;
						sll.add_node(employid,position);
					}
					else {
						cout<<"cannot add to the linked list it exceeds size"<<endl;
					}
					break;

			case 2:		cout<<"enter the accno to delete"<<endl;
					int deletenum;
					cin>>deletenum;
					sll.deleteEmployeeid(deletenum);
					break;
			case 3: 	cout<<"enter the position the node to be updated"<<endl;

					cin>>position;
					sll.update(position);

			case 4:		sll.displayEmployeeid();	
					break;

			default :       cout<<"enter the valid number"<<endl;

		}
	}	




}



/*******************************************************************
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  1
  enter the position the node to be added
  0
  enter the employe id to be added
  1
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  1
  enter the position the node to be added
  1
  enter the employe id to be added
  2
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  4
  The Employee id is: 1
  The Employee id is: 2
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  2
  enter the accno to delete
  1
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  4
  The Employee id is: 2
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  3
  enter the position the node to be updated
  1
  enter the data to be update:
  5
  The Employee id is: 5
  what operation do you want to perform
  1.Add
  2.Delete
  3.Update
  4.Display

  3
  enter the position the node to be updated
  1
  enter the data to be update:
6
The Employee id is: 6
what operation do you want to perform
1.Add
2.Delete
3.Update
4.Display



***************************************************************************/






