/****************************************************************************
Author:

Bhaskar Pedapudi	Emp id:10362	cell No: 99665396470
---------------------
purpose:
---------------------

This program is to create a Stack perform Some operations on it

 ****************************************************************************/


#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<iostream>
#include<unistd.h>

//creating a structure for the data and self referential structure
struct EmployeeDetails {
		int employid;
		//creating self referential structure
		EmployeeDetails *next;
		};

//globally declared count variable
static int count=0;

using namespace std;

	
class Stack{
	
	//this pointer is used to store the starting address of the stack	
	struct EmployeeDetails *head=NULL;

public:
	/*this method is used for the data to be push on the stack
	  this method is similar to add at the last
	 */
	 void push(int data) {
			//creating memory for structured members
			EmployeeDetails  *temp = new EmployeeDetails ;
			EmployeeDetails  *temp1;
			if(head==NULL) {
				temp->next=head;
				head=temp;
				count =count+1;
				temp->employid = data;
			}
			else {
				temp1=head;
				for(temp1=head;temp1->next!=NULL;temp1=temp1->next);
				temp1->next=temp;
				temp->next=NULL;
				temp->employid = data;
				count=count+1;
				
			}
	}

	
	/*this method is used for the data to be pop from the stack
	  this method is similar to delete at the end
	 */

	void pop() {
			if(head==NULL) {
				cout<<"Stack is empty"<<endl;
			}
			if(head->next==NULL) {
				head=NULL;
			}
			
			//these two pointers are for traversing
			EmployeeDetails *temp=NULL;
			EmployeeDetails *prev=temp;
			if(head!=NULL) {
			for( temp=head;temp->next!=NULL;prev=temp,temp=temp->next);
			prev->next=NULL;
			temp=NULL;
			}
		}

	//this function is used to display all the elements in the queue
	
	void displayStackElements() {
			EmployeeDetails *temp=head;
			if(temp==NULL) {
				cout<<"The stack is empty"<<endl;
			}
			while(temp!=NULL) {
				cout<<"The employee id is:"<<temp->employid<<endl;
				temp=temp->next;
			}
			
		}

	
	/*this method is used to search the corresponding employee id
	  if exists it returns the corresponding index
	  if not it prints some error message
	 */

	void search(int num) {
			
			EmployeeDetails *temp=head;
			int c=0;
			int flag=0;
			while(temp!=NULL) {
				c++;
				if(temp->employid==num) {
					flag=1;
					cout<<"The seaching Employee  is found at "<< (c-1) <<"th position"<<endl;
					}
				temp=temp->next;
			}
			if(flag==0) {
				cout<<"the acc no is not found"<<endl;
			}
			
	} 
		
	};



	int main(void) {
			//creating object for Stack class
			Stack s;

			while(true)
			{
				cout<<"what operation do you want to perform in Stack"<<endl;
				cout<<"1.Push 2.Pop 3.Display 4.Search "<<endl;
				int number;
				cin>>number;
			switch(number) {
							
			case 1:		cout<<"enter the Employee id to add"<<endl;;
					int employeeid;
					cin>>employeeid;
					s.push(employeeid);
					break;
					
			case 2:		s.pop();
					break;
					
			case 3:		s.displayStackElements();	
					break;
					
			case 4: 	cout<<"enter the acc number to be searched"<<endl;
					int num;
					cin>>num;
					s.search(num);
					break;
				
			
			default: cout<<"enter the correct number"<<endl;		
				}
			}
			
			
		}
	
		


/**************************************************************************************

OUTPUT:

what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 
1
enter the Employee id to add
10362
what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 
1
enter the Employee id to add
10363
what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 
3
The employee id is:10362
The employee id is:10363
what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 
2
what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 
3
The employee id is:10362
what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 
4
enter the acc number to be searched
10362
The seaching Employee  is found at 0th position
what operation do you want to perform in Stack
1.Push 2.Pop 3.Display 4.Search 








********************************************************************************/
