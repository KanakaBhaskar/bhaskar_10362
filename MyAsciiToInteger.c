/*************************************************************
  P.KANAKA BHASKAR  Emp-Id:10362  pedapudibhaskar@gmail.com  9966539647

This program is to convert the ascii to integer or ascii to float based on the input given.
We have checked all the input validations.
************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

//This function is used to convert the ascii to float
float myatof(char *str) {
	int i,number=0,count=0,flag=0;
	float fnumber=0.0;
	//this is another logic for ascii to float
	/*for(i=0;i<strlen(str);i++) {
	  while(str[i]!='.') {
	  number=number*10+(str[i]-48);
	  i++;
	  }
	  if(str[i]=='.') {
	  i++;
	  break;
	  }
	  }
	  for(;i<strlen(str);i++) {
	  count++;
	  number=number*10+(str[i]-48);
	  }*/
	for(i=0;i<strlen(str);i++) {
		if(str[i]=='-' && i==0) {
		}
		else if(str[i]!='.' && str[i]>='0'&&str[i]<='9') {
			number=number*10+(str[i]-48);
		}
		else {
			if(str[i]=='.') {
				flag++;
				if(flag==1) {
					count=i;
					continue;
				}
				else {
					printf("entered number is invalid\n");
					exit(0);
				}
			}
			else {
				printf("entered number is invalid\n");
				exit(0);
			}
		}
	}
	fnumber = number/pow(10,(i-count-1));
	return fnumber;
}

//This function is used to convert the ascii to integer
int myatoi(char *str) {
	int i,number=0;
	for(i=0;i<strlen(str);i++) {
		if(str[i]=='-' && i==0) {
			continue;
		}
		else if(str[i]>='0' && str[i]<='9'){
			number=number*10+(str[i]-48);
		}
		else {
			printf("entered number is invalid\n");
			exit(0);
		}
	}
	return number;
}

int main() {

	char str[10];//character array to take the input
	int i,flag=0;//instant variables
	printf("enter the value:");
	scanf("%s",str);

	for(i=0;i<strlen(str);i++) {
		if(str[i]=='.')
			flag++;
		else continue;
	}
	if(flag) 
	{
		float fnum=myatof(str);
		if(str[0]=='-') 
			printf("number=-%f\n",fnum);
		else
			printf("number=%f\n",fnum);
	}
	else {
		int inum=myatoi(str);
		if(str[0]=='-') 
			printf("number=-%d\n",inum);
		else
			printf("number=%d\n",inum);
	}
}

/***********************************************************

OUTPUT:
enter the value:1234.56
number=1234.560059
user@user:~/practiceclass.c/selfish/strings$ ./a.out
enter the value:-1234.56
number=-1234.560059
user@user:~/practiceclass.c/selfish/strings$ ./a.out
enter the value:-123.4.5
entered number is invalid
user@user:~/practiceclass.c/selfish/strings$ ./a.out
enter the value:12345
number=12345
user@user:~/practiceclass.c/selfish/strings$ ./a.out
enter the value:-123-0
entered number is invalid

*************************************************************/
