/*************************************************************
  P.KANAKA BHASKAR    Emp-Id:10362     pedapudibhaskar@gmail.com  Ph No:9966539647

  This program is to find the fibonacci series of a given number

 ************************************************************/

#include<stdio.h>
#include<stdlib.h>

//logic for fibonacci series using reccursion

int fibRecursion(int num) {

	if(num==0) {
		return 0;
	}
	if(num==1) {
		return 1;
	}
	return fibRecursion(num-1)+fibRecursion(num-2);

}


int main() {
	//instant variables
	int num,i;
	printf("enter the number\n");
	scanf("%d",&num);
	//printing the series
	for(i=0;i<=num;i++) {
		int result = fibRecursion(i);
		printf("%d ",result);
	}
	printf("\n");
	return 0;
}

/***********************************************************
OUTPUT:

enter the number
5
0 1 1 2 3 5 

 *****************************************************/
