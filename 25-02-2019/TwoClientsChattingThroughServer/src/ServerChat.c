#include"Server.h"

//error function to print the error message
void error(char *msg){
	perror(msg);
	exit(1);
}

//in amin method we create socket, bind the ipaddress and port number and we read, write data continuously in child and parent process
int main(int argc, char **argv){
	int sockfd,newSockfd,portNum,newSockfd1;
	char buf[1024];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	if(newSockfd<0)error("ERROR IN ACCEPTING");

	newSockfd1 = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	if(newSockfd1<0)error("ERROR IN ACCEPTING");
	 int ret=-1;
	 ret=fork();
	 if(ret == 0)
	 {
	       	while(1){
		       memset(buf,0,sizeof(buf));
		       n = read(newSockfd1,buf,1024);
		       printf("client= %s\n",buf);
		       if(n<0)
			       error("ERROR ON READING");
		       n = write(newSockfd,buf,strlen(buf));
		       if(n<0)
			       error("ERROR ON WRITING");
			}
       }
	 else
	 {		
		 while(1)
		 {
		       memset(buf,0,sizeof(buf));
		       n = read(newSockfd,buf,1024);
		       if(n<0)
			       error("ERROR ON READING");
		       printf("client= %s\n",buf);
		       n = write(newSockfd1,buf,strlen(buf));
		       if(n<0)
			       error("ERROR ON WRITING");
		 }
	 }
       close(newSockfd);
       close(newSockfd1);
       close(sockfd);
return 0;
}
