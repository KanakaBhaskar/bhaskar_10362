#include<headers.h>

//initialising the instant variables
int sockfd,newsockfd,portnum;
char buf[4]={'b','y','e','\n'};
//character buff of size 1024
char buff[1024];
//declaring the thread id's
pthread_t thread_id,thread_id1;

//this function is to read the data from client and print on the console
//if the client sends bye then it terminates
void *serverToread(void * arg) {
	while(1) {
	//clearing the data
	memset(buff,0,sizeof(buff));
	//reading the data from the newsockfd
	int n = read(newsockfd,buff,1024);
	if(n<0) {
		error("error in reading\n");
		exit(0);
	}
	//checking condition for termination
	if(!(strcmp(buf,buff))) {
		exit(0);
	}
	else {
		printf("%s\n",buff);
	}
	}
	return NULL;
}
//this function is to send the data to the client 
//if it sends bye then client terminates

void *serverTowrite(void * arg) {
	while(1) {
	//clearing the data
	memset(buff,0,sizeof(buff));
	//reading the data from user
	int n = read(0,buff,1024);
	if(n<0) {
		error("error in writing\n");
		exit(0);
	}
	//checking condition for termination
	if(!(strcmp(buf,buff))) {
		write(newsockfd,buff,strlen(buff));
		exit(0);
	}
	else {
		
		write(newsockfd,buff,strlen(buff));
	}
	}
	return NULL;
}

//this function is to print the error message
void error(char *msg) {
	printf("%s",msg);
	exit(0);
}

int main(int argc, char **argv) {
	//inbuilt structure declaration
	struct sockaddr_in serv_addr,cli_addr;
	//this is for storing the size of client address
	socklen_t clilen;
	//creating the socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
	error("error in creation of socket\n");
	exit(0);
	}
	//collecting the portno
	portnum = atoi(argv[1]);
	//initialising the all structure variables
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portnum);
	
	//binding the server socket
	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) <0) {
		error("error in binding\n");
		exit(0);
	}
	//listening
	listen(sockfd,5);
	clilen=sizeof(cli_addr);
	//accepting from the server
	newsockfd= accept(sockfd,(struct sockaddr *)&cli_addr,&clilen);

	if(newsockfd < 0) {
		error("error in accepting\n");
		exit(0);
	}
	//pthread creation
	pthread_create(&thread_id,NULL,serverTowrite,NULL);
	pthread_create(&thread_id1,NULL,serverToread,NULL);

		
	//joining of pthreads
	pthread_join(thread_id,NULL);
	pthread_join(thread_id1,NULL);

	//closing of socket file descriptors
	close(newsockfd);
	close(sockfd);

	return 0;
}



		
