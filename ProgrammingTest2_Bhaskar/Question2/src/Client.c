#include<headers.h>
//intialising the variables
int sockfd,portnum;
char buf[4]={'b','y','e','\n'};
char buff[1024];
//declaring the thread variables
pthread_t thread_id,thread_id1;

//this function is to read the data from client and print it on console
//if server sends bye then it terminates
void *clientToread(void * arg) {
	while(1) {
	memset(buff,0,sizeof(buff));
	int n = read(sockfd,buff,1024);
	if(n<0) {
		error("error in reading\n");
		exit(0);
	}
	
	if(!(strcmp(buf,buff))) {
	//	printf("%s\n",buff);
		exit(1);
	}
	else {
		printf("%s\n",buff);
	}
	}
	return NULL;
}

//this function is to write data to the server
//if it sends bye then it terminates
void *clientTowrite(void * arg) {
	while(1) {
	memset(buff,0,sizeof(buff));
	//int n = read(0,buff,strlen(buff));
	fgets(buff,1024,stdin);
	int n=write(sockfd,buff,strlen(buff));
	if(n<0) {
		error("error in writing\n");
		exit(0);
	}
	if(!(strcmp(buf,buff))) {
		//write(sockfd,buff,strlen(buff));
		printf("bye\n");
		exit(1);
	}

	}
	return NULL;
}
//this function is to print the error message
void error(char *msg) {
	printf("%s",msg);
	exit(0);
}

int main(int argc, char **argv) {
	//declaring the pre-defined structure variables
	struct sockaddr_in serv_addr;
	struct hostent *server;

	//creating the socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
	error("error in creation of socket\n");
	exit(0);
	}
	//collecting the localhost ip address
	server = gethostbyname(argv[1]);

	portnum = atoi(argv[2]);
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	//initialisng the structrued variables
	serv_addr.sin_family = AF_INET;
	//serv_addr.sin_addr.s_addr = INADDR_ANY;
	strncpy((char *)&server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portnum);
	
	//connecting the client and server
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {
		error("error in connection\n");
		exit(0);
	}

	//creation of pthreads

	pthread_create(&thread_id,NULL,clientTowrite,NULL);
	pthread_create(&thread_id1,NULL,clientToread,NULL);

	//joining of pthreads
	pthread_join(thread_id,NULL);
	pthread_join(thread_id1,NULL);

	//closing the socket file descriptor
	close(sockfd);

	return 0;
}




