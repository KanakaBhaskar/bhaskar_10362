#include<headers.h>

void error(char *msg) {
	printf("%s",msg);
	exit(0);
}

int main(int argc, char **argv) {
	int sockfd,portnum,size=0;
	char *buff,str[20];
	//declaring the pre-defined structure variables
	struct sockaddr_in serv_addr;
	struct hostent *server;
	
	FILE *fp;
	//creating the socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
	error("error in creation of socket\n");
	exit(0);
	}
	//collecting the localhost ip address
	server = gethostbyname(argv[1]);

	portnum = atoi(argv[2]);
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	//initialisng the structrued variables
	serv_addr.sin_family = AF_INET;
	//serv_addr.sin_addr.s_addr = INADDR_ANY;
	strncpy((char *)&server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portnum);
	
	//connecting the client and server
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {
		error("error in connection\n");
		exit(0);
	}

	printf("enter the file name\n");
	//gets(str);
	scanf("%s",str);	
	//read(0,str,sizeof(str));
	write(sockfd,str,strlen(str));
	sleep(2);
	read(sockfd,&size,sizeof(int));
	buff=(char *)malloc(size*sizeof(char));
	read(sockfd,buff,size);
	fp=fopen(str,"w");
	fwrite(buff,size,1,fp);
	printf("file is successfully recieved\n");
	//closing of file
	fclose(fp);

	return 0;
}




