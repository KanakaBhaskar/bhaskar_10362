#include<headers.h>
 
	
//this function is to print the error message
void error(char *msg) {
	printf("%s",msg);
	exit(0);
}

int main(int argc, char **argv) {
	int portnum,sockfd,newsockfd,size=0;
	//inbuilt structure declaration
	struct sockaddr_in serv_addr,cli_addr;
	//this is for storing the size of client address
	socklen_t clilen;
	char *buff,file[10]={'\0'};
	FILE *fp;
	//creating the socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
	error("error in creation of socket\n");
	exit(0);
	}
	//collecting the portno
	portnum = atoi(argv[1]);
	//initialising the all structure variables
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portnum);
	
	//binding the server socket
	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) <0) {
		error("error in binding\n");
		exit(0);
	}
	//listening
	listen(sockfd,5);
	clilen=sizeof(cli_addr);
	//accepting from the server
	newsockfd= accept(sockfd,(struct sockaddr *)&cli_addr,&clilen);

	if(newsockfd < 0) {
		error("error in accepting\n");
		exit(0);
	}
	//reading the file name from the user
	read(newsockfd,file,10);
	//assiging null to it
	file[strlen(file)]='\0';
	//opening the file in read mode
	fp=fopen(file,"r");
	fseek(fp,0,2);
	//find the size of the file
	size=ftell(fp);
	rewind(fp);
	sleep(2);
	//write the size of the file
	write(newsockfd,&size,sizeof(size));
	//allocating the memory
	buff=(char *)malloc(size*sizeof(int));
	fread(buff,size,1,fp);
	//writing the data into the newsockfd
	write(newsockfd,buff,size);
	fclose(fp);

	return 0;
}
