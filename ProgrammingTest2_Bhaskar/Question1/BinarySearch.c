#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

//declaration of binary search function
int binarysearch(int ,int,int* ,int);

//This function receives arguements as lowest index, highest index,array and the data to be searched.
int binarysearch(int low,int high,int *ptr,int data) {
	//declaration of instant variables
	int mid;
	mid=(low+high)/2;//calculating the mid value
	//the termination condition 
	if(low>high) {
		return -1;
	}
	//if data is equal to searched data then return the index value
	else if(ptr[mid]==data) {
		return mid;
	}
	//this condition is for to traverse left of the array
	else if(ptr[mid]>data) {
		high=mid-1;
		return binarysearch(low,high,ptr,data);
		
	}
	//this condition is for to traverse right of the array
	else {
		low=mid+1;
		return binarysearch(low,high,ptr,data);
		
	}
	

}

//entry point to the function

int main() {
	//array intialisation
	int array[10]={1,2,5,6,8,9,10,11,13,14};
	//declaring the instant variables
	int size,data,index;
	//asking the user to enter the data to be searched
	printf("enter the data to be searched\n");
	scanf("%d",&data);
	//calculating the size of the array
	size=sizeof(array)/sizeof(array[0]);
	//calling the binarysearch function and collecting it to another variable
	index=binarysearch(0,size-1,array,data);
	if(index == -1) {
		printf("the data is not found\n");
	}
	else {
		printf("the data is found at %dth position\n",index);
	}
	return 0;
}
