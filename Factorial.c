/**************************************************
  P.KANAKA BHASKAR  Emp-Id:10362        pedapudibhaskar@gmail.com Ph No
  : 9966539647

  This program is to find the factorial of a given number by using recursion
********************************************************/



#include<stdio.h>
#include<stdlib.h>

//This function is calculating the factorial of a given

int fact(int num) {

	if(num == 0)
		return 1;
	else
		return num*fact(num-1);
}

int main() {
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int result = fact(num);
	printf("factorial of %d is %d\n",num,result);
}

/*****************************************************
OUTPUT:
enter the number
5
factorial of 5 is 120

****************************************************/
