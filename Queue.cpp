/****************************************************************************
Author:

Bhaskar Pedapudi	Emp id:10362	cell No: 99665396470
---------------------
purpose:
---------------------

This program is to create a Queue perform Some operations on it

 ****************************************************************************/





#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<iostream>
#include<unistd.h>


//creating a structure for the data and self referential structure
struct EmployeeDetails {
	int employid;
	//creating self referential structure
	EmployeeDetails *next;
};

//globally declared count variable
static int count=0;

using namespace std;


class Queue {

	//this pointer is used to store the starting address of the single linked list	
	struct EmployeeDetails *head = NULL;

	public:
	/*this method is used for the data to be put on the queue
	  this method is similar to add at the last
	 */
	void enque(int data) {
		//creating memory for the structured members
		EmployeeDetails *temp = new EmployeeDetails;
		EmployeeDetails *temp1;	
		if(head==NULL) {
			temp->next=head;
			head=temp;
			temp->employid = data;
			count =count+1;
		}
		else {
			temp1=head;
			for(temp1=head;temp1->next!=NULL;temp1=temp1->next);
			temp1->next=temp;
			temp->next=NULL;
			temp->employid = data;
			count=count+1;

		}
	}

	/*this method is used for the data to be remove from the queue
	  this method is similar to delete at the begin
	 */

	void deque() {
		if(head==NULL) {
			cout<<"queue is empty"<<endl;
		}
		head=head->next;

	}


	/*this method is used to search the corresponding employee id
	  if exists it returns the corresponding index
	  if not it prints some error message
	 */

	void search(int num) {

		EmployeeDetails *temp = head;
		int c=0;
		int flag=0;
		while(temp!=NULL) {
			c++;
			if(temp->employid==num) {
				flag=1;
				cout<<"The seaching employe id is found at "<< (c-1) <<"th position"<<endl;
			}
			temp=temp->next; 
		}
		if(flag==0) {
			cout<<"the acc no is not found"<<endl;
		}

	} 

	//this function is used to display all the elements in the queue
	void displayQueue() {
		EmployeeDetails *temp = head;
		if(temp==NULL) {
			cout<<"The Queue is Empty"<<endl;
		}
		while(temp!=NULL) {
			cout<<"The employee id is: "<<temp->employid<<endl;
			temp=temp->next;
		}

	}

};



int main(void) {
	//creating object for queue class
	Queue queue;
	while(true)
	{
		cout<<"what operation do you want to perform in Queue"<<endl;
		cout<<"1.Enque 2.Deque 3.Display 4.Search "<<endl;
		int number;
		cin>>number;
		switch(number) {

			case 1:		cout<<"enter the employ id no to add"<<endl;
					int employid;
					cin>>employid;
					queue.enque(employid);
					break;

			case 2:		queue.deque();
					break;

			case 3:		queue.displayQueue();	
					break;

			case 4: 	cout<<"enter the employ to be searched"<<endl;
					int num;
					cin>>num;
					queue.search(num);
					break;


			default: 	cout<<"enter the correct number"<<endl;		
		}
	}


}


/**********************************************************************************

OUTPUT:

what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 
1
enter the employ id no to add
10363what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 
1
enter the employ id no to add
10363
what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 
1
enter the employ id no to add
10362
what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 
3
The employee id is: 10363
The employee id is: 10362
what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 
2
what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 
3
The employee id is: 10362
what operation do you want to perform in Queue
1.Enque 2.Deque 3.Display 4.Search 

 ********************************************************************************/




