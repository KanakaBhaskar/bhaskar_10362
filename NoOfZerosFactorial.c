/***********************************************************
AUTHOR:

P.KANAKA BHASKAR   Emp-Id:10362   pedapudibhaskar@gmail.com  9966539647

This program is to find the number of zeros in the given factorial number

***********************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>


int main() {

	//instant variables
	int data,result=0,count=1;

	printf("enter the data\n");
	scanf("%d",&data);

	//logic for finding the no.of zeros in the factorial number

	while(data >= pow(5,count)) {
		result = result+(data/pow(5,count));
		count++;
	}
	printf("The no.of zeros in the %d factorial are:%d\n",data,result);
	return 0;
}

/**************************************************
OUTPUT:

enter the data
100
The no.of zeros in the 100 factorial are:24
user@user:~/practiceclass.c$ ./a.out
enter the data
213
The no.of zeros in the 213 factorial are:51

******************************************************/
