/********************************************************
AUTHOR: 

P.KANAKA BHASKAR   Emp-Id:10362	   pedapudibhaskar@gmail.com  9966539647

This program is to find roots of a quadratic equation

 ********************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main() {

	int a,b,c;//These are the coefficients for variables and constant
	float root1,root2,root,root3,root4;//these are instant variables for store root values

	printf("enter a,b and c values\n");
	scanf("%d%d%d",&a,&b,&c);

	root1= -b;
	root2=(b*b-(4*a*c));

	//condition for imaginary roots
	if(root2<0) {
		root2 = -root2;
		root3 = root1/(2*a);
		root4 = sqrt(root2)/(2*a);
		printf("the two roots are :%f+i%f\n and %f-i%f\n",root3,root4,root3,root4);
	}

	//condition for real roots
	else {
		root3 = (root1+sqrt(root2))/(2*a);
		root4 = (root1-sqrt(root2))/(2*a);
		printf("the two roots are :%f\n and %f\n",root3,root4);
	}

	return 0;
}

/********************************************************
OUTPUT:

enter a,b and c values
3
5
2
the two roots are :-0.666667
and -1.000000
user@user:~/practiceclass.c$ ./a.out
enter a,b and c values
5
6
7
the two roots are :-0.600000+i1.019804
and -0.600000-i1.019804
 *****************************************************/
